using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private float _speed = 1f;
    [SerializeField] private GameObject ExplosionPrefab;
    private float _direction = 0;
    private bool _init = true;
    private Rigidbody2D _rb;
    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
    }
    private void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        if (_init)
        {
            _rb.AddForce(new Vector2(_speed * _direction, 0), ForceMode2D.Impulse);
        }
        _init = false;
    }

    public void SetDirection(float direction)
    {
        _direction = direction;
    }

    public void AddGravity()
    {
        _rb.gravityScale = 1;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Instantiate(ExplosionPrefab, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}
