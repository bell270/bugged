using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    private int _shotCount = 0;
    [SerializeField] private GameObject BulletPrefab;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Shoot(float direction, bool bugged)
    {
        GameObject newBullet = Instantiate(BulletPrefab, transform.position, Quaternion.identity);
        newBullet.GetComponent<Bullet>().SetDirection(direction);
        if (bugged)
        {
            newBullet.GetComponent<Bullet>().AddGravity();
        }
        _shotCount++;
    }
}
