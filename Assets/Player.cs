using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private CharacterController2D _controller;
    private LineRenderer _lr;
    private float _movement = 0f;
    [SerializeField] private float _runSpeed = 40f;
    public bool _isJumping;
    public bool _isCrouching;
    private GameObject _gun;

    public LayerMask PlayerLayer;
    public LayerMask GroundLayer;

    public GameObject WinScreen;
    public GameObject GlitchScreen;

    [SerializeField] private byte _bugCount = 0;
    [SerializeField] private float _teleportDistance = 10f;
    private bool pickupDone = false;
    private bool _wasBugged = false;

    private void Start()
    {
        _controller = GetComponent<CharacterController2D>();
        _gun = GetComponentInChildren<Gun>().gameObject;
        _lr = GetComponentInChildren<LineRenderer>();
        _bugCount = 0;
    }
    // Update is called once per frame
    void Update()
    {
        Glitch();
        _movement = Input.GetAxis("Horizontal") * _runSpeed;

        if(Input.GetButtonDown("Jump"))
        {
            _isJumping = true;
        }
        if(Input.GetKeyDown(KeyCode.E))
        {
            _gun.GetComponent<Gun>().Shoot(transform.localScale.x, (_bugCount >= 1 * 64));
        }

        if (_bugCount == 0 && _wasBugged)
        {
            WinScreen.SetActive(true);
            Time.timeScale = 0;
        }
        else
        {
            WinScreen.SetActive(false);
        }
    }

    private void Glitch()
    {
        float glitchChanse = UnityEngine.Random.Range(0f, 1f);
        GlitchScreen.SetActive(false);
        if (glitchChanse < _bugCount / 64 * 0.05)
        {
            GlitchScreen.SetActive(true);
        }
    }

    public void OnGround()
    {
        _isJumping = false;
    }

    private void FixedUpdate()
    {
        if (_isJumping && _bugCount >= 2 * 64)
        {
            Vector3 newPosition = transform.position + new Vector3(_movement * Time.fixedDeltaTime, _teleportDistance, 0);
            transform.position = newPosition;
        }
        Physics2D.IgnoreLayerCollision((int)Math.Log(PlayerLayer.value, 2), (int)Math.Log(GroundLayer.value, 2), (_bugCount >= 3 * 64));
        _controller.Move(_movement * Time.fixedDeltaTime, _isCrouching, (_isJumping && !(_bugCount >= 2 * 64)));
        _isJumping = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        BUG bug = collision.gameObject.GetComponent<BUG>();
        if (bug != null)
        {
            if (!pickupDone)
            {
                _wasBugged = true;
                   _bugCount+=64;
                Destroy(bug.gameObject);
                pickupDone = true;
                Invoke("ResetPickup", 0.2f);
            }
        }
    }
    void ResetPickup()
    {
        pickupDone = false;
    }
}
